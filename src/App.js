import React from 'react'
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from 'react-router-dom'
import styled from 'styled-components'
import BGimage from './assets/map-of-the-world-2401458_1920.jpg'
import MainPage from './containers/MainPage'
import ChangeAdress from './containers/ChangeAdress'
import routes from './router/routes'
import GetQuery from './containers/GetQuery'

const App = () => {
  return (
    <Router>
      <BgStyle>
        <Switch>
          <Route path={`/:tid/:pid/:secret_key`}>
            <GetQuery/>
          </Route>
          <Route path={routes.mainPage}>
            <MainPage/>
          </Route>
          <Route path={routes.dropOff}>
            <ChangeAdress/>
          </Route>
        </Switch>
      </BgStyle>
    </Router>
  )
}

export default App

const BgStyle = styled.div`
  width: 100vw;
  height: 100%;
  min-height: 100vh;
  background-image: linear-gradient(rgba(255,255,255,0.5), rgba(255,255,255,0.6)), url(${BGimage});
  background-repeat: no-repeat;
  background-size: cover;
  background-position-x: center;
  display: flex;
  flex-direction: column;
  justify-content: center;
`
