import { UPLOAD_DETAILS, SET_LOADING } from '../types'

export const uploadDetails = (DB) => ({
    type: UPLOAD_DETAILS,
    payload: DB,
})

export const setLoading = (bool) => ({
    type: SET_LOADING,
    payload: bool,
})