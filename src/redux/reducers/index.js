import { UPLOAD_DETAILS, SET_LOADING } from '../types'
import { currentText } from '../../language'

const initialState = {
    loading: true
}

const isRTL = (language) => {
    switch (language) {
        case 'he':
            return true
        case 'ar':
            return true
        case 'fa':
            return true
        case 'ur':
            return true
        default:
            return false
    }
}

const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPLOAD_DETAILS:
            return {
                ...state, 
                menu: action.payload.Menu,
                MainMessage: action.payload.MainMessage,
                twoColumnsMenu: action.payload.EnableTwoColumnsForMenu,
                appConfig: {
                    ...action.payload.AppConfiguration,
                    HeaderTextColor: `#${action.payload.AppConfiguration.HeaderTextColor.slice(2)}`,
                    HeaderBackgroundColor: `#${action.payload.AppConfiguration.HeaderBackgroundColor.slice(2)}`,
                    ContentTextColor: `#${action.payload.AppConfiguration.ContentTextColor.slice(2)}`,
                    ContentIconsColor: `#${action.payload.AppConfiguration.ContentIconsColor.slice(2)}`,
                    ActionButtonsTextColor: `#${action.payload.AppConfiguration.ActionButtonsTextColor.slice(2)}`,
                    ActionButtonsBackgroundColor: `#${action.payload.AppConfiguration.ActionButtonsBackgroundColor.slice(2)}`,
                },
                RTL: isRTL(action.payload.AppConfiguration.Language),
                languagePage: currentText[action.payload.AppConfiguration.Language],
                deliveryDet: action.payload.DeliveryDetails,
                titlePage: action.payload.Title,
                feedbackForm: action.payload.FeedbackForm,
                mapData: action.payload.MapData
            }
        case SET_LOADING:
            return {...state, loading: action.payload}
        default:
            return state
    }
}

export default rootReducer