import qs from 'qs'

let data = ( secret_key ) => qs.stringify({
    'client_id': '35bda4df-a052-4b24-8423-428372f38252',
    'scope': 'https://prod.applicat.com/.default',
    'client_secret': secret_key,
    'grant_type': 'client_credentials' 
})

export const configAccessToken = (secret_key) => ({
  method: 'post',
  url: 'https://cors-anywhere.herokuapp.com/https://login.microsoftonline.com/hormigatest.onmicrosoft.com/oauth2/v2.0/token',
  headers: { 
    'Content-Type': 'application/x-www-form-urlencoded',
    'Cookie': 'x-ms-gateway-slice=estsfd; stsservicecookie=estsfd; fpc=AizorenmaY9Ig3vV0eP8qwNjavKlAQAAAO54UNcOAAAA'
  },
  data : data(secret_key)
});

export const configGetData = (tid, pid, token) => ({
        method: 'get',
        url: `https://cors-anywhere.herokuapp.com/https://hormigatest.applicat.com/api/CXWeb/GetPage/${tid}/${pid}`,
        headers: { 
            'Authorization': `Bearer ${token}`
        }
});

export const configPostAddress = ( tid, pid, token, body ) => ({
        method: 'post',
        url: `https://cors-anywhere.herokuapp.com/https://hormigatest.applicat.com/api/CXWeb/SetAddress/${tid}/${pid}`,
        headers: { 
            'Authorization': `Bearer ${token}`
        },
        data: body
})