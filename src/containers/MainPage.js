import React from 'react'
import { useSelector } from 'react-redux'

import Map from '../components/Map'
import FeedbackForm from '../components/FeedbackForm'
import DeliveryDetails from '../components/DeliveryDetails'
import OptionsMainAll from '../components/OptionsMainAll'
import OptionsMainSmall from '../components/OptionsMainSmall'
import MainContact from '../components/MainContact'
import NavBar from '../components/NavBar'
import MainContainer from './MainContainer'

const MainPage = () => {
    const {loading, mapData, feedbackForm, titlePage} = useSelector(s => s)
    
    const isComponents = (map, feedback) => {
        if(map !== null) return (
            <>
                <DeliveryDetails/>
                <OptionsMainSmall/>
                <Map/>
                <MainContact/>
            </>
        )
        if(feedback !== null) return (
            <>
                <DeliveryDetails/>
                <FeedbackForm/>
            </>
        )
        if(feedback === null && map === null) return (
            <>
                <DeliveryDetails/>
                <OptionsMainAll/>
            </>
        )
    }

    const components = loading ? '' : isComponents(mapData, feedbackForm);

    return (
        <MainContainer>
            <NavBar isMain={true} titlePage={titlePage}/>
            {components}
        </MainContainer>
    )
}

export default MainPage

