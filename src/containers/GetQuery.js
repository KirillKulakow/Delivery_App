import { Redirect, useParams } from 'react-router-dom'
import routes from '../router/routes'

const GetQuery = () => {
    const { tid, pid, secret_key } = useParams()
    localStorage.setItem('tid', tid)
    localStorage.setItem('pid', pid)
    localStorage.setItem('secret_key', secret_key)

    return <Redirect to={routes.mainPage}/>
}

export default GetQuery
