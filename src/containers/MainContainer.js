import React, { useCallback, useEffect } from 'react'
import styled from 'styled-components'
import { useDispatch, useSelector } from 'react-redux'
import axios from 'axios'
import Loader from '../components/Loader'
import { setLoading, uploadDetails } from '../redux/actions'
import { configAccessToken, configGetData } from '../utils/axios'

const MainContainer = ({children}) => {
    const dispatch = useDispatch()
    const { loading, appConfig, RTL } = useSelector(s => s)
    const tid = localStorage.getItem('tid')
    const pid = localStorage.getItem('pid')
    const secret_key = localStorage.getItem('secret_key')

    const loadData = useCallback(async () => {
        dispatch(setLoading(true))
        if(!localStorage.getItem('token')) {
            await axios(configAccessToken(secret_key))
            .then(res => {
                localStorage.setItem('token', res.data.access_token)
                setTimeout(() => {
                    localStorage.removeItem('token')
                }, 30*(1000*60))
            })
            .catch(e => console.log(e))
        }
        await axios(configGetData(tid, pid, localStorage.getItem('token')))
            .then(res => {
                dispatch(uploadDetails(res.data))
                dispatch(setLoading(false))
            })
            .catch(e => console.log(e))
    }, [dispatch, pid, tid, secret_key]);

    useEffect(() => {
        loadData()
    }, [dispatch, loadData])

    return (
        loading ? <Loader/> : 
        <Container textcolor={appConfig.ContentTextColor} isRTL={RTL}>
            {children}
        </Container>
    )
}

export default MainContainer

const Container = styled.div`
    background-color: #e5e5e5;
    max-width: 475px;
    min-width: 320px;
    margin: 0 auto;
    color: ${({textcolor}) => (textcolor)};
    direction: ${({isRTL}) => (isRTL ? 'rtl' : 'ltr')};
`
