const routes = {
    mainPage: '/main',
    moreOptions: '/options',
    packageDetails: '/details',
    dropOff: '/drop',
    updateDeliveryDate: '/date_set',
    pickUpPoint: '/pick_up_set',
    pickUpPointDetails: '/pick_up_details',
    neighbor: '/neighbor_set',
    outsideDoor: '/outside_set',
    storeVacation: '/vacation_set',
    sendMessageToCourier: '/send_message',
    sendEmail: '/send_email',
    process_payment: '/payment',
    contactUs: '/contacts',
    confirmUpdateDeliveryDate: '/',
    errorPage: '/error'
}

export default routes