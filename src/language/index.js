export const currentText = {
    en: {
        DeliveryDetails: {
            Consignor: 'Consignor',
            Delivery_time: 'Delivery time',
            Message: 'Dear :Consignee:, delivery no. :TrackingNumber: from :Consignor: is pending payment with a total sum of X USD',
            dayOfWeek: [
                'Sunday',
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday'
            ]
        },
        Options: {
            OptionsTitle: 'You may perform the folowing actions',
            MoreOptions: 'More options'
        },
        FeedbackForm: {
            CommentsQuestion: 'Do you have any other comments?',
            Button: 'Send your feedback',
            FeedbackRangeLike: 'Like',
            FeedbackRangeUnLike: 'Can be improved'
        },
        Contact: {
            Title: 'Feel free to contact us at any time',
            Support: 'Customer Support',
            Coureier: 'Contact Courier'
        },
        StatusArray: [
            'Picked up',
            'Ready for delivery',
            'On the way',
            'Delivered'
        ]
    },
    ar: {

    },
    he: {
        DeliveryDetails: {
            Consignor: 'שולח',
            Delivery_time: 'זמן משלוח',
            Message: 'Dear :Consignee:, delivery no. :TrackingNumber: from :Consignor: is pending payment with a total sum of X USD',
            dayOfWeek: [
                'יוֹם רִאשׁוֹן',
                 'יוֹם שֵׁנִי',
                 'יוֹם שְׁלִישִׁי',
                 'יום רביעי',
                 'יוֹם חֲמִישִׁי',
                 'יוֹם שִׁישִׁי',
                 'יום שבת'
            ]
        },
        Options: {
            OptionsTitle: 'תוכל לבצע את הפעולות הבאות',
            MoreOptions: 'אפשרויות נוספות'
        },
        FeedbackForm: {
            CommentsQuestion: 'האם יש לך הערות אחרות?',
            Button: 'שלח את המשוב שלך',
            FeedbackRangeLike: 'כמו',
            FeedbackRangeUnLike: 'יכול להשתפר'
        },
        Contact: {
            Title: 'אתם מוזמנים ליצור איתנו קשר בכל עת',
            Support: 'שירות לקוחות',
            Coureier: 'צרו קשר עם שליח'
        },
        StatusArray: [
            'נאסף',
            'מוכן למשלוח',
            'בדרך',
            'נמסר'
        ]
    },
    fa: {

    },
    ur: {

    }
}