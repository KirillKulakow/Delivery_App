import styled from 'styled-components'
import Sprite from '../assets/Sprite_png.png'

const Icon = styled.div`
    width: 35px;
    height: 35px;
    float: left;
    background: url(${Sprite}) no-repeat;
    transform: scale(0.8);
`
const backObject = () => {
    let array = [];
    let stringY = '';
    let stringX = '';
    let yNumber = -34;
    for(let i = 0; i <= 29; i++){
        if(Number.isInteger((i + 0 )/ 3)) {
            yNumber = yNumber + 42;
            stringY = `-${yNumber}px`
        }
        if(i === 0) {stringX = `-16px`}
        else if(i === 1) {stringX = `-73px`}
        else if(i === 2) {stringX = `-127px`}
        else if(Number.isInteger((i + 0 )/ 3)) {stringX = `-16px`}
        else if(Number.isInteger((i + 1 )/ 3) || i === 1) {stringX = `-127px`}
        else if(Number.isInteger((i + 2 )/ 3) || i === 2) {stringX = `-73px`}
        array.push({i: 
           styled(Icon)`background-position: ${stringX + " " + stringY};`
        })
    }
    return array
}
const IconsArray = backObject();

export const IconsObject = {
    PayIcon: IconsArray[0].i,
    ClockIcon: IconsArray[1].i,
    MessageIcon: IconsArray[2].i,
    LocateIcon: IconsArray[3].i,
    PersonIcon: IconsArray[4].i,
    CallbackIcon: IconsArray[5].i,
    CalendarIcon: IconsArray[6].i,
    ToHomeIcon: IconsArray[7].i,
    CallIcon: IconsArray[8].i,
    NeibghorIcon: IconsArray[9].i,
    CalendarDateIcon: IconsArray[10].i,
    SupportIcon: IconsArray[11].i,
    OutsideIcon: IconsArray[12].i,
    ShopIcon: IconsArray[13].i,
    GeoPositionIcon: IconsArray[14].i,
    MarkerBoxIcon: IconsArray[15].i,
    BoxIcon: IconsArray[16].i,
    MessageCircleIcon: IconsArray[17].i,
    VacationIcon: IconsArray[18].i,
    CertificateIcon: IconsArray[19].i,
    CallSupportIcon: IconsArray[20].i,
    ContactIcon: IconsArray[21].i,
    PayCardIcon: IconsArray[22].i,
    AddIcon: IconsArray[23].i,
    TimerIcon: IconsArray[24].i,
    PayDocIcon: IconsArray[25].i,
    DeliveryIcon: IconsArray[26].i,
    MobileIcon: IconsArray[27].i,
    CashIcon: IconsArray[28].i,
    AirPlaneIcon: IconsArray[29].i,
}