import React from 'react'
import { useSelector } from 'react-redux'
import { Link } from "react-router-dom"
import styled from 'styled-components'
import routes from '../router/routes'
import { IconsObject } from '../styledIcons'
import { IconContext } from "react-icons";
import { IoIosArrowForward, IoIosArrowBack } from 'react-icons/io'

export const findOptionText = (array, ID) => {
    let result = array.find(object => object.ID === ID);
    return result.Text
}

const OptionsMainAll = () => {
    const { RTL, menu, appConfig } = useSelector(s => s);
    const menuTitles = menu.filter(object => object.MenuTypeID === 2)
    const buttonsOptions = menu.filter(object => object.MenuTypeID !== 2)
    // const buttonsOther = menu.filter(object => object.ID >= 5)
    const { PayIcon, LocateIcon, CalendarIcon, NeibghorIcon, OutsideIcon, MarkerBoxIcon, VacationIcon, ContactIcon } = IconsObject
    return (
        <>
            <OptionsTitle>
                <p>{findOptionText(menuTitles, 2)}</p>
            </OptionsTitle>
            <OptionsLink to={routes.process_payment} textcolor={appConfig.ContentTextColor} isRTL={RTL}>
                <p>{findOptionText(buttonsOptions, 3)}</p>
                <PayIcon style={RTL ? {position: 'absolute', right: '0.5rem', top: '0.65rem'} : {position: 'absolute', left: '0.5rem', top: '0.65rem'}}/>
                <IconContext.Provider value={{size: '2rem', color: '#bfbfbf'}}>
                    {RTL ? <ArrowIconBack/> : <ArrowIcon/>}
                </IconContext.Provider>
            </OptionsLink>
            <OptionsTitle>
                <p>{findOptionText(menuTitles, 4)}</p>
            </OptionsTitle>
            <OptionsGrid isRTL={RTL}>
                {/* {buttonsOther.map(item => (
                    <OptionsLinkGrid key={item.ID}>
                        <p>{item.Text}</p>
                        <LocateIcon style={{position: 'absolute', left: '0.5rem', top: '0.65rem'}}/>
                    </OptionsLinkGrid>
                ))} */}
                <OptionsLinkGrid to={routes.dropOff} textcolor={appConfig.ContentTextColor} isRTL={RTL}>
                    <p>{findOptionText(buttonsOptions, 5)}</p>
                    <LocateIcon style={RTL ? {position: 'absolute', right: '0.5rem', top: '0.65rem'} : {position: 'absolute', left: '0.5rem', top: '0.65rem'}}/>
                </OptionsLinkGrid>
                <OptionsLinkGrid to='/' textcolor={appConfig.ContentTextColor} isRTL={RTL}>
                    <p>{findOptionText(buttonsOptions, 6)}</p>
                    <CalendarIcon style={RTL ? {position: 'absolute', right: '0.5rem', top: '0.65rem'} : {position: 'absolute', left: '0.5rem', top: '0.65rem'}}/>
                </OptionsLinkGrid>
                <OptionsLinkGrid to='/' textcolor={appConfig.ContentTextColor} isRTL={RTL}>
                    <p>{findOptionText(buttonsOptions, 7)}</p>
                    <NeibghorIcon style={RTL ? {position: 'absolute', right: '0.5rem', top: '0.65rem'} : {position: 'absolute', left: '0.5rem', top: '0.65rem'}}/>
                </OptionsLinkGrid>
                <OptionsLinkGrid to='/' textcolor={appConfig.ContentTextColor} isRTL={RTL}>
                    <p>{findOptionText(buttonsOptions, 8)}</p>
                    <OutsideIcon style={RTL ? {position: 'absolute', right: '0.5rem', top: '0.65rem'} : {position: 'absolute', left: '0.5rem', top: '0.65rem'}}/>
                </OptionsLinkGrid>
                <OptionsLinkGrid to='/' textcolor={appConfig.ContentTextColor} isRTL={RTL}>
                    <p>{findOptionText(buttonsOptions, 9)}</p>
                    <MarkerBoxIcon style={RTL ? {position: 'absolute', right: '0.5rem', top: '0.65rem'} : {position: 'absolute', left: '0.5rem', top: '0.65rem'}}/>
                </OptionsLinkGrid>
                <OptionsLinkGrid to='/' textcolor={appConfig.ContentTextColor} isRTL={RTL}>
                    <p>{findOptionText(buttonsOptions, 10)}</p>
                    <VacationIcon style={RTL ? {position: 'absolute', right: '0.5rem', top: '0.65rem'} : {position: 'absolute', left: '0.5rem', top: '0.65rem'}}/>
                </OptionsLinkGrid>
                <OptionsLinkGrid to='/' textcolor={appConfig.ContentTextColor} isRTL={RTL}>
                    <p>{findOptionText(buttonsOptions, 11)}</p>
                    <ContactIcon style={RTL ? {position: 'absolute', right: '0.5rem', top: '0.65rem'} : {position: 'absolute', left: '0.5rem', top: '0.65rem'}}/>
                </OptionsLinkGrid>
            </OptionsGrid>
        </>
    )
}

export default OptionsMainAll

const OptionsTitle = styled.h4`
    padding: 0 0.8rem;
    height: 3rem;
    font-weight: 400;
    border-bottom: 2px solid #bfbfbf;
    display: flex;
    align-items: center;
`
const OptionsLink = styled(Link)`
    display: block;
    padding: 0 2.5rem;
    height: 3.2rem;
    border-bottom: 2px solid #bfbfbf;
    font-size: 0.85rem;
    position: relative;
    text-decoration: none;
    color: ${({textcolor}) => (textcolor)};
    display: flex;
    align-items: center;

`
const ArrowIcon = styled(IoIosArrowForward)`
    position: absolute;
    right: 0.4rem;
    top: 0.65rem;
`
const ArrowIconBack = styled(IoIosArrowBack)`
    position: absolute;
    left: 0.4rem;
    top: 0.65rem;
`
const OptionsGrid = styled.div`
    display: grid;
    grid-template-columns: repeat(2, 1fr);
    direction: ${({isRTL}) => (isRTL ? 'rtl' : 'ltr')};
`
const OptionsLinkGrid = styled(OptionsLink)`
    padding: ${({isRTL}) => (isRTL ? '0 2.7rem 0 0.5rem' : '0 0.5rem 0 2.7rem')};
    line-height: 1rem;
    display: flex;
    align-items: center;
    &:nth-child(odd){
        ${({isRTL}) => (!isRTL ? 'border-right: 2px solid #bfbfbf;' : 'border-left: 2px solid #bfbfbf;')}
    }
`
