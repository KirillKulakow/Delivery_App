import React from 'react'
import styled from 'styled-components'
import { Link } from "react-router-dom"
import { IconContext } from "react-icons"
import { IoIosArrowForward, IoIosArrowBack } from 'react-icons/io'
import { IconsObject } from '../styledIcons'
import { useSelector } from 'react-redux'

import { findOptionText } from './OptionsMainAll'

const OptionsMainSmall = () => {
    const { PayIcon, LocateIcon, MarkerBoxIcon, AddIcon} = IconsObject
    const { languagePage: {Options}, RTL, menu } = useSelector(s => s)

    return (
        <Container>
            <OptionsTitle>
                <p>{Options.OptionsTitle}</p>
                <IconContext.Provider value={{size: '1.7rem', color: '#bfbfbf'}}>
                    {RTL ? <ArrowIconBack/> : <ArrowIcon/>}
                </IconContext.Provider>
            </OptionsTitle>
            <GridTemplate>
                <OptionsLinkGrid to='/' isRTL={RTL}>
                    <p>{findOptionText(menu, 3)}</p>
                    <PayIcon style={RTL ? {position: 'absolute', right: '0.5rem', top: '0.3rem'} : {position: 'absolute', left: '0.5rem', top: '0.3rem'}}/>
                </OptionsLinkGrid>
                <OptionsLinkGrid to='/' isRTL={RTL}>
                    <p>{findOptionText(menu, 9)}</p>
                    <MarkerBoxIcon style={RTL ? {position: 'absolute', right: '0.5rem', top: '0'} : {position: 'absolute', left: '0.5rem', top: '0'}}/>
                </OptionsLinkGrid>
                <OptionsLinkGrid to='/' isRTL={RTL}>
                    <p>{findOptionText(menu, 5)}</p>
                    <LocateIcon style={RTL ? {position: 'absolute', right: '0.5rem', top: '0.3rem'} : {position: 'absolute', left: '0.5rem', top: '0.3rem'}}/>
                </OptionsLinkGrid>
                <OptionsLinkGrid to='/' isRTL={RTL}>
                    <p>{Options.MoreOptions}</p>
                    <AddIcon style={RTL ? {position: 'absolute', right: '0.5rem', top: '0.3rem'} : {position: 'absolute', left: '0.5rem', top: '0.3rem'}}/>
                </OptionsLinkGrid>
            </GridTemplate>
        </Container>
    )
}

export default OptionsMainSmall

const Container = styled.div`
    width: 92%;
    margin: 0 auto 1rem;
    padding-bottom: 0.5rem;
    background-color: #fff;
`
const OptionsTitle = styled.h4`
    padding: 0 0.8rem;
    height: 2.5rem;
    font-weight: 400;
    font-size: 0.89rem;
    display: flex;
    align-items: center;
    position: relative;
`
const ArrowIcon = styled(IoIosArrowForward)`
    position: absolute;
    right: 0;
    top: 50%;
    transform: translateY(-50%);
`
const ArrowIconBack = styled(IoIosArrowBack)`
    position: absolute;
    left: 0;
    top: 50%;
    transform: translateY(-50%);
`
const GridTemplate = styled.div`
    display: grid;
    grid-template-columns: 1fr 1fr;
`
const OptionsLinkGrid = styled(Link)`
    height: 2.3rem;
    font-size: 0.75rem;
    text-decoration: none;
    line-height: 0.8rem;
    color: #000;
    display: flex;
    align-items: center;
    position: relative;
    padding: ${({isRTL}) => (isRTL ? '0 2.7rem 0 0' : '0 0 0 2.7rem')};
    display: flex;
    align-items: center;
`