import React from 'react'
import styled from 'styled-components'
import { IoIosArrowForward, IoIosArrowBack } from 'react-icons/io'
import { IconContext } from "react-icons";
import { useHistory } from 'react-router-dom';
import { useSelector } from 'react-redux'

const NavBar = ({ isMain, titlePage }) => {
    const { goBack }= useHistory()
    const {appConfig, RTL: isRTL } = useSelector(s => s)
    return (
        <Nav color={appConfig.HeaderBackgroundColor} isMain={isMain ? '' : 'padding-left: 3rem'}>
            {isMain ? 
            <>
                <Logo src={appConfig.Logo} position={isRTL ? 'right' : 'left'}/>
                {titlePage ? <Title color={appConfig.HeaderTextColor}>{titlePage}</Title> : ''}
            </>
            :
            <>
                <IconContext.Provider value={{size: '2rem', color: '#bfbfbf'}}>
                        {!isRTL ? <ArrowIcon onClick={goBack}/> : <ArrowIconBack onClick={goBack}/>}
                </IconContext.Provider>
                <Title style={{}}color={appConfig.HeaderTextColor}>{titlePage}</Title>
            </>
            }
        </Nav>
    )
}

export default NavBar

const Nav = styled.nav`
    width: 100%;
    ${({isMain}) => (isMain)};
    margin: 0 auto;
    height: 50px;
    position: relative;
    background-color: ${({color}) => (color)};
    box-shadow: 0 0 10px rgba(0,0,0,0.5);
    display: flex;
    align-items: center;
    justify-content: center;
    box-sizing: border-box;
`
const Logo = styled.img`
    max-width: 90px;
    max-height: 50px;
    position: absolute;
    top: 50%;
    ${({position}) => (position === 'left' ? 'left: 13px;' : 'right: 13px;')}
    transform: translateY(-50%);
    background-size: 60px auto;
    background-repeat: no-repeat;
`
const Title = styled.p`
    font-weight: 600;
    font-size: 1.1rem;
    color: ${({color}) => (color)};
`
const ArrowIcon = styled(IoIosArrowForward)`
    position: absolute;
    right: 0.4rem;
    top: 0.65rem;
    cursor: pointer;
`
const ArrowIconBack = styled(IoIosArrowBack)`
    position: absolute;
    left: 0.4rem;
    top: 0.65rem;
    cursor: pointer;
`