import React from 'react'
import styled from 'styled-components'
import StatusBar from './StatusBar'
import { useSelector } from 'react-redux'

const DeliveryDetails = () => {
    const { deliveryDet, appConfig, languagePage, RTL, MainMessage } = useSelector(s => s)
    const { ActionButtonsTextColor } = appConfig

    const dayOfWeek = languagePage.DeliveryDetails.dayOfWeek

    const dateToString = (date) => {
        let _date = new Date(date);
        return `${dayOfWeek[_date.getUTCDay()]}, ${_date.getDate()}/${_date.getMonth() + 1}/${_date.getFullYear()}`
    }

    const timeToString = (fromDate, beforeDate) => {
        const formatTime = (time) => {
            if(time.toString().length <= 1) return `0${time}`
            return time.toString()
        }
        const time = (date) => {
            return `${formatTime(date.getHours())}:${formatTime(date.getMinutes())}`
        }
        return `${time(new Date(fromDate))} - ${time(new Date(beforeDate))}`
    }

    return (
        <>
        <ContainerDetails>
            <ColumnFirst isRTL={RTL}>
                <Title>{languagePage.Consignor}</Title>
                <ConsignorContainer>
                    <p>{deliveryDet.Consignor}</p>
                    <p>{deliveryDet.TrackingNumber}</p>
                </ConsignorContainer>
                <StatusBar statusID={deliveryDet.StatusID} appConfig={appConfig} isRTL={RTL}/>
            </ColumnFirst>
            <ColumnSecond isRTL={RTL}>
                <Title>{languagePage.Delivery_time}</Title>
                <p>{dateToString(deliveryDet.ScheduledDate)}</p>
                <p>{timeToString(deliveryDet.ScheduledStartTime, deliveryDet.ScheduledEndTime)}</p>
            </ColumnSecond>
        </ContainerDetails>
        <ContainerMessage color={ActionButtonsTextColor} dangerouslySetInnerHTML={{__html: 
        MainMessage}}>
        </ContainerMessage>
        </>
    )
}

export default DeliveryDetails

const ContainerDetails = styled.div`
    display: grid;
    padding: 0.55rem 0.8rem;
    grid-template-columns: 1fr 1fr;
    font-size: 0.75rem;
`
const ColumnFirst = styled.div`
    text-align: ${({isRTL}) => (isRTL ? 'right' : 'left')};

`
const ColumnSecond = styled.div`
    text-align: ${({isRTL}) => (isRTL ? 'left' : 'right')};
`
const Title = styled.h4`
    font-weight: 600;
    margin-bottom: 3px;
`
const ConsignorContainer = styled.div`
    margin-bottom: 0.9rem;
`
const ContainerMessage = styled.div`
    width: 87%;
    margin: 0 auto 15px;
    padding: 0.5rem;
    background-color: ${({color}) => (color)};
    font-size: 0.75rem;
    color: #fff;
`
