import React from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'

const MainContact = () => {
    const { RTL, 
        mapData: { MapMessage, ContactUsButtonPhone, ShowContactUsButton },
        appConfig: { ActionButtonsTextColor, ActionButtonsBackgroundColor },
        languagePage: { Contact }
    } = useSelector(s => s)
    
    return (
        <Container>
            <Text>
                <p>{MapMessage}</p>
                <p>{Contact.Title}</p>
            </Text>
            <ContainerButtons bgColor={ActionButtonsBackgroundColor} color={ActionButtonsTextColor} isRTL={RTL}>
                <a href='/'>{Contact.Support}</a>
                {ShowContactUsButton && <a href={ContactUsButtonPhone}>{Contact.Coureier}</a>}
            </ContainerButtons>
        </Container>
    )
}

export default MainContact

const Container = styled.div`
    width: 92%;
    margin: 0 auto 10px;
`
const Text = styled.div`
    font-size: 0.85rem;
    margin-bottom: 1.3rem;
    font-weight: 600;
`
const ContainerButtons = styled.div`
    display: flex;
    justify-content: space-around;
    font-size: 0.9rem;
    border-collapse: collapse;
    a {
        width: 100%;
        border: 2px solid ${({color}) => (color)};
        text-align: center;
        padding: 0.5rem 0;
        text-decoration: none;
        color: ${({color}) => (color)};
        background-color: #fff;
        font-weight: 600;
    }
    a:nth-child(2) {
        ${({isRTL}) => (isRTL ? "border-right: none;" : "border-left: none;")}
    }
`