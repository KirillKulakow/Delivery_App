import React from 'react'
import styled from 'styled-components'

const StatusBar = ({isRTL, statusID, appConfig}) => {
    const { ActionButtonsTextColor } = appConfig;

    let statusArray = [
        'Picked up',
        'Ready for delivery',
        'On the way',
        'Delivered'
    ]
    const getColor = (id) => {
        if (statusID >= id) return ActionButtonsTextColor
        return '#8f8f8f'
    }
    return (
        <>
        <StatusContainer>
            <Dot1 color={isRTL ? getColor(3) : getColor(0)}/>
            <Bar1 color={isRTL ? getColor(3) : getColor(1)}/>
            <Dot2 color={isRTL ? getColor(2) : getColor(1)}/>
            <Bar2 color={isRTL ? getColor(2) : getColor(2)}/>
            <Dot3 color={isRTL ? getColor(1) : getColor(2)}/>
            <Bar3 color={isRTL ? getColor(1) : getColor(3)}/>
            <Dot4 color={isRTL ? getColor(0) : getColor(3)}/>
        </StatusContainer>
        <Info>{statusArray[statusID]}</Info>
        </>
    )
}

export default StatusBar

const StatusContainer = styled.div`
    position: relative;
    width: 100px;
    height: 10px;
    margin: 0 0 5px 5px;
`
const Bar = styled.div`
    height: 1px;
    background-color: ${({color}) => (color)};
    width: calc(100% / 3);
    position: absolute;
    top: 50%;
    transform: translateY(-50%);
`
const Bar1 = styled(Bar)`
    left: 0;
`
const Bar2 = styled(Bar)`
    left: calc(100% / 3);
`
const Bar3 = styled(Bar)`
    left: calc(100% / 3 * 2);
`
const Dot = styled.div`
    height: 10px;
    width: 10px;
    border-radius: 50%;
    background-color: ${({color}) => (color)};
    position: absolute;
    top: 50%;
    transform: translate(-50%, -50%);
    z-index: 3;
`
const Dot1 = styled(Dot)`
    left: 0;
`
const Dot2 = styled(Dot)`
    left: calc(100% / 3);
`
const Dot3 = styled(Dot)`
    left: calc(100% / 3 * 2);
`
const Dot4 = styled(Dot)`
    left: 100%;
`
const Info = styled.p`
    font-size: 0.75rem;
`