import React, { useState } from 'react'
import styled from 'styled-components'
import { BsFillStarFill } from 'react-icons/bs'

const FeedbackRangeStars = ({setStarRating, id}) => {
    const [rateState, setRateState] = useState({
        rating: null,
        tempRating: null
    })

    const rate = (rating) => {
        setRateState({
            rating: rating,
            tempRating: rating
        })
        setStarRating(id, rating + 1)
    }

    const starOver = (rating) => {
        const prev = rateState.rating;
        setRateState({
            rating: rating,
            tempRating: prev
        })
    }

    const starOut = () => {
        setRateState({
            ...rateState,
            rating: rateState.temp_rating,
        })
    }

    let stars = []

    for(let i = 0; i < 5; i++) {
        let Component = StarLabel
        if(rateState.rating >= i && rateState.rating != null){
            Component = StarLabelSelected
        }
        stars.push(
            <Component
                onClick={() => rate(i)}
                onMouseOver={() => starOver(i)}
                onMouseOut={() => starOut()}
                key={i}
            >
                <BsFillStarFill/>
            </Component>
        )
    }

    return (
        <StarsContainer>
            {stars}
        </StarsContainer>
    )
}

export default FeedbackRangeStars

const StarLabel = styled.div`
    display: inline-block;
    width: 100%;
    text-align: center;
    vertical-align: middle;
    line-height: 1;
    font-size: 1.5em;
    color: #ABABAB;
    transition: color .2s ease-out;
    z-index: 2;
    &:hover {
      cursor: pointer;
    }
`
const StarLabelSelected = styled(StarLabel)`
    color: #FFD700;
`
const StarsContainer = styled.div`
    width: 70%;
    margin: 0 auto 0.8rem;
    display: flex;
    justify-content: space-around;
`