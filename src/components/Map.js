import React from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import GoogleMapReact from 'google-map-react';
import { IconContext } from "react-icons"
import { FaMapMarker, FaTruck } from 'react-icons/fa'
import { BsHouseFill } from 'react-icons/bs'

const HomeMarker = () => (
    <HomeMarkerContainer>
        <IconContext.Provider value={{size: '2rem', color: '#3d3d3d'}}>
            <FaMapMarker/>
        </IconContext.Provider>
        <IconContext.Provider value={{size: '0.9rem', color: '#fff'}}>
            <BsHouseFill style={{position: 'absolute', top: '0.2rem', left: '62%', transform: 'translateX(-50%)'}}/>
        </IconContext.Provider>
    </HomeMarkerContainer>)
    
const CourierMarker = ({color}) => (
    <CourierMarkerContainer>
        <IconContext.Provider value={{size: '2rem', color: color}}>
            <FaMapMarker/>
        </IconContext.Provider>
        <IconContext.Provider value={{size: '0.9rem', color: '#fff'}}>
            <FaTruck style={{position: 'absolute', top: '0.25rem', left: '52%', transform: 'translateX(-50%)'}}/>
        </IconContext.Provider>
    </CourierMarkerContainer>);

const Map = () => {
    const { mapData, deliveryDet, appConfig: {ActionButtonsTextColor} } = useSelector(s => s)
    const HomeLocation = {lat: deliveryDet.Address.GeoLatitude, lng: deliveryDet.Address.GeoLongitude}
    const CourierLocation = {lat: mapData.DriverLatitude, lng: mapData.DriverLongitude}
    const API_KEY_GOOGLE_MAPS = process.env.REACT_APP_API_KEY_GOOGLE_MAPS;

    const defaultProps = {
        center: {
          lat: CourierLocation.lat,
          lng: CourierLocation.lng
        },
        zoom: 5
    };
    return (
        <MapContainer>
            <GoogleMapReact
            bootstrapURLKeys={{ key: API_KEY_GOOGLE_MAPS }}
            defaultCenter={defaultProps.center}
            defaultZoom={defaultProps.zoom}
            >
            <CourierMarker color={ActionButtonsTextColor}
                lat={CourierLocation.lat}
                lng={CourierLocation.lng}
            />
            <HomeMarker
                lat={HomeLocation.lat}
                lng={HomeLocation.lng}
            />
            </GoogleMapReact>
        </MapContainer>
    )
}

export default Map

const MapContainer = styled.div`
    height: 11rem;
    width: 92%;
    margin: 0 auto 10px;
`
const HomeMarkerContainer = styled.div`
    width: 25px;
    height: 25px;
    position: absolute;
    bottom: -100%;
    right: -50%;
    transform: translateX(50%);
`
const CourierMarkerContainer = styled.div`
    width: 2rem;
    height: 2rem;
    position: absolute;
    bottom: -100%;
    right: -50%;
    transform: translateX(50%);
`