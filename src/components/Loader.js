import React from 'react'
import styled from 'styled-components'

const Loader = () => {
    return (
        <BGLoader>
            <Spinner>
                <BubbleFirst/>
                <BubbleSecond/>
            </Spinner>
        </BGLoader>
    )
}

export default Loader



const BGLoader = styled.div`
    height: 100vh;
    width: 100vw;
    position: fixed;
    left: 0;
    top: 0;
    right: 0;
    bottom: 0;
    background-color: #000;
    opacity: 0.7;
    display: flex;
    justify-content: center;
    align-items: center;
`
const Spinner = styled.div`
    position: relative;
    width: 45px;
    height: 45px;
    margin: 0 auto;
    animation: loadingI 2s linear infinite;
    @keyframes loadingI {
        100% {
            transform: rotate(360deg);
        }
    }
`
const BubbleFirst = styled.div`
    position: absolute;
    top: 0;
    width: 25px;
    height: 25px;
    border-radius: 100%;
    background-color: #42f5e3;
    animation: bounce 2s ease-in-out infinite;
    @keyframes bounce {
        0%,
        100% {
            transform: scale(0);
        }
        50% {
            transform: scale(1);
        }
    }
`
const BubbleSecond = styled(BubbleFirst)`
    top: auto;
    bottom: 0;
    animation-delay: -1s;
`