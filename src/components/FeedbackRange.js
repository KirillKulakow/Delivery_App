import React, { useState } from 'react'
import styled from 'styled-components'

const FeedbackRange = ({language, isRTL, setStarRating, id, children}) => {
    const [rangeValue, setRangeValue] = useState('0');
    const handleChange = (e) => {
        setRangeValue(e.target.value)
        setStarRating(id, e.target.value)
    }

    return (
        <RangeContainer>
            <Title>{children}</Title>
            <Range 
                type="range" 
                min="-1" max="1" 
                value={rangeValue} 
                onChange={handleChange}
                step="1"/>
              {isRTL ? 
                <>
                <RightDesc>{language.FeedbackRangeUnLike}</RightDesc>
                <LeftDesc>{language.FeedbackRangeLike}</LeftDesc>
                </>
                :
                <>
                <LeftDesc>{language.FeedbackRangeUnLike}</LeftDesc>
                <RightDesc>{language.FeedbackRangeLike}</RightDesc>
                </>
              }
            
        </RangeContainer>
    )
}

export default FeedbackRange

const Range = styled.input`
  -webkit-appearance: none;
  width: 80%;
  height: 2px;
  border-radius: 5px;
  background: #d7dcdf;
  outline: none;
  padding: 0;
  margin: 0;
  position: relative;

  &::-webkit-slider-thumb {
    appearance: none;
    position: relative;
    width: 25px;
    height: 25px;
    border: 2px solid #2c3e50;
    border-radius: 50%;
    background: #fff;
    cursor: pointer;
    transition: background 0.15s ease-in-out;
    z-index: 2;
  }

  &::-moz-range-thumb {
    width: 20px;
    height: 20px;
    border: 0;
    border-radius: 50%;
    background: #2c3e50;
    cursor: pointer;
    transition: background 0.15s ease-in-out;
  }

  &:focus {
    &::-webkit-slider-thumb {
      box-shadow: 0 0 0 3px #fff, 0 0 0 6px #00A2FF;
    }
  }
  &:before, 
  &:after{
      content: '';
      width: 10px;
      height: 10px;
      border-radius: 50%;
      background-color: #d7dcdf; 
      position: absolute;
      top: 50%;
  }
  &:before{
    left: 0;
    transform: translate(-50%, -50%)
  }
  &:after{
    right: 0;
    transform: translate(50%, -50%)
  }
`
const Title = styled.p`
    padding: 0.9rem 0;
    font-size: 0.9rem;
    margin-bottom: 1rem;
`
const RangeContainer = styled.div`
    display: flex;
    flex-direction: column;
    align-items: center;
    position: relative;
    padding-bottom: 2.5rem;
    border-bottom: 1px solid #bfbfbf;
`
const Desc = styled.span`
    position: absolute;
    bottom: 15%;
    font-size: 0.7rem;
`
const LeftDesc = styled(Desc)`
    left: 10%;
`
const RightDesc = styled(Desc)`
    right: 10%;
`