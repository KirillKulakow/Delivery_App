import React, { useState } from 'react'
import { useSelector } from 'react-redux'
import styled from 'styled-components'
import FeedbackRange from './FeedbackRange'
import FeedbackRangeStars from './FeedbackRangeStars'

const FeedbackForm = () => {
    const { RTL, feedbackForm, appConfig, languagePage } = useSelector(s => s)
    const initialState = feedbackForm.map(e => ({
        ID: e.ID,
        Value: '0'
    }))
    const [_feedbackForm, set_FeedbackForm] = useState(initialState)
    const setCurrentRate = (id, rat) => {
        let rate = _feedbackForm.find(e => e.ID === id)
        let value = []
        if (!rate) {
            value = [
                ..._feedbackForm,
                {
                    ID: id,
                    Value: rat
                }
            ]
        } else {
            value = _feedbackForm.map(e => {
                if(e.ID === id) return {
                    ID: id,
                    Value: rat
                }
                return e
            })
        }
        set_FeedbackForm(value)
    }
    return (
        <FeedbackContainer>
            {feedbackForm.map((element) => {
                if (element.IsTotalRating) return (
                    <TitleContainer key={element.ID}>
                        <FeedbackTitle key={element.ID}>
                            {element.Text}
                        </FeedbackTitle>
                        {element.DisplayIndex === 1 ? 
                        <FeedbackRangeStars 
                            feedbackForm={_feedbackForm} 
                            setStarRating={setCurrentRate} 
                            id={element.ID}
                        /> 
                        : ''}
                    </TitleContainer>
                )
                return (
                <FeedbackRange 
                    key={element.ID} 
                    feedbackForm={_feedbackForm} 
                    setStarRating={setCurrentRate} 
                    id={element.ID}
                    isRTL={RTL}
                    language={languagePage.FeedbackForm}>
                        {element.Text}
                </FeedbackRange>)
            })}
            <FeedbackSubTitle>{languagePage.FeedbackForm.CommentsQuestion}</FeedbackSubTitle>
            <FeedbackTextArea>
                <textarea rows="5" cols="45"></textarea>
            </FeedbackTextArea>
            <SubmitButton color={appConfig.ContentIconsColor}>{languagePage.FeedbackForm.Button}</SubmitButton>
        </FeedbackContainer>
    )
}

export default FeedbackForm

const FeedbackContainer = styled.form`
    margin-bottom: 10px;
`
const FeedbackTitle = styled.p`
    width: 100%;
    text-align: center;
    padding: 0.8rem 0;
    font-size: 1rem;
`
const FeedbackSubTitle = styled.p`
    width: 100%;
    padding: 0.5rem 1rem;
    font-size: 0.9rem;
`
const TitleContainer = styled.div`

`
const FeedbackTextArea = styled.div`
    display: flex;
    justify-content: center;
    margin-bottom: 5px;
    textarea {
        width: 90%;
        border-radius: 5px;
    }
`

const SubmitButton = styled.button`
    width: 93%;
    height: 2.7rem;
    display: block;
    margin: 0 auto;
    border: none;
    background-color: ${({color}) => (color)};
    font-size: 0.9rem;
    font-weight: 600;
    color: #fff;
    cursor: pointer;
`